# Final Project

## Kelompok 8

## Anggota Kelompok

- Lina Rufaidah
- Zidan Aqila Muhammad

## Tema Project

Website Social Media

## ERD

<img src="public/images/erd.png" alt="ERD">

## Link Website

https://meet-upgram.herokuapp.com/

## Link Video

https://bit.ly/Meet-Upgram