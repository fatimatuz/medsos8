<!doctype html>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Meet-Upgram</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/bootstrap.min.css')}}">
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/animate.css')}}">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/owl.carousel.min.css')}}">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/all.css')}}">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('tourbi/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('tourbi/css/nice-select.css')}}">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/magnific-popup.css')}}">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/slick.css')}}">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{asset('tourbi/css/style.css')}}">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container mt-2">
            <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand"> <img src="{{asset('tourbi/img/icon/feature_icon_3.png')}}" alt="logo"> </a>
                        
                        <ul class="navbar-nav">
                                <li class="nav-item text-secondary">
                                    <a class="nav-link" href="/register"><h4>Register</h4></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/login"><h4>Login</h4></a>
                                </li>
                         </ul>
        </nav>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h5>Best way to meet and happy</h5>
                            <h5>Mari bergabung!</h5>
                            <h1>Meet-Upgram</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!--::footer_part start::-->
    <footer class="footer_part">
        <div class="container">
            <hr>
            <div class="row">
                <div class="col-lg-8">
                    <div class="copyright_text">
                        <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer_icon social_icon">
                        <ul class="list-unstyled">
                            <li><a href="#" class="single_social_icon"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fab fa-behance"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <img src="img/overlay_2.png" alt="#" class="footer_overlay">
    </footer>
    <!--::footer_part end::-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="{{asset('tourbi/js/jquery-1.12.1.min.js')}}"></script>
    <!-- popper js -->
    <script src="{{asset('tourbi/js/popper.min.js')}}"></script>
    <!-- bootstrap js -->
    <script src="{{asset('tourbi/js/bootstrap.min.js')}}"></script>
    <!-- easing js -->
    <script src="{{asset('tourbi/js/jquery.magnific-popup.js')}}"></script>
    <!-- particles js -->
    <script src="{{asset('tourbi/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('tourbi/js/jquery.nice-select.min.js')}}"></script>
    <!-- slick js -->
    <script src="{{asset('tourbi/js/slick.min.js')}}"></script>
    <script src="{{asset('tourbi/js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('tourbi/js/waypoints.min.js')}}"></script>
    <script src="{{asset('tourbi/js/contact.js')}}"></script>
    <script src="{{asset('tourbi/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('tourbi/js/jquery.form.js')}}"></script>
    <script src="{{asset('tourbi/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('tourbi/js/mail-script.js')}}"></script>
    <!-- custom js -->
    <script src="{{asset('tourbi/js/custom.js')}}"></script>
</body>

</html>