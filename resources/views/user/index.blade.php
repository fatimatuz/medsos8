@extends('layouts.master')

@section('cardside')
<div class="col-md-2">
<h3><a style="color:blue"><b>Meet-Up</b></a><a style="color:red">gram</a></h3>
<h5> All Users </h5>
</div>
@endsection

@section ('main')

<table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Follow/Unfollow</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($users as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->username}}</td>
                        <td>{{$value->email}}</td>
                        <td>
                        @if ($value->id !== Auth::user()->id)
                        @if (Auth::user()->show_follow($value->id) == null)
                        <form action="/follow" method="POST" class="float-right">
                        @csrf
                        <button type="submit" class="btn btn-primary" name="id" value="{{$value->id}}">Follow</button>
                        </form>
                        @else
                        <form action="/follow/{{Auth::user()->show_follow($value->id)->id}}" method="POST" class="float-right">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-outline-primary">Following</button>
                        </form>
                        @endif
                        @endif
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>Tidak ada data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        
@push('script')
<script type="text/javascript" src="{{ asset('js/custom.js') }}" defer></script>
@endpush
@endsection