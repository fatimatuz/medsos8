@extends('layouts.master')

@section('main')

<div class="m-4">
  <form action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="caption">Caption</label>
        <textarea class="form-control" name="caption" id="caption" rows="3">{{$post->caption}}</textarea>
    </div>
    @error('caption')
      <div class="alert alert-danger mt-2">
        {{ $message }}
      </div>
    @enderror
    <div class="form-group">
      <label for="image">Image</label>
      <input type="file" class="form-control-file" name="image" id="image" value="{{asset('images/'.$post->image)}}">
    </div>
    <button type="submit" class="btn btn-primary">Post</button>
  </form>
</div>

@endsection