@extends('layouts.master')

@section('main')

<div class="m-4">
  <form action="/post" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="caption">Caption</label>
        <textarea class="form-control" name="caption" id="caption" rows="3"></textarea>
    </div>
    @error('caption')
      <div class="alert alert-danger mt-2">
        {{ $message }}
      </div>
    @enderror
    <div class="form-group">
      <label for="image">Image</label>
      <input type="file" class="form-control-file" name="image" id="image">
    </div>
    <button type="submit" class="btn btn-primary">Post</button>
  </form>
</div>

@endsection