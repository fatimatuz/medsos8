@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="" class="h1"><b>Meet-</b>Upgram</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg"><b>Register a new membership</b></p>

      <form action="{{route('register')}}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="username" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        @error('username')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
        <input type="text" class="form-control" name="name" placeholder="Nama Lengkap">
        </div>
        @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        <div class="input-group mb-3">
        <textarea class="form-control" name="bio" placeholder="Biodata kamu"></textarea>
        </div>
        @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        <div class="input-group mb-0">
        <p>Isi Tanggal Lahir:</p>
        </div>
        <div class="input-group mb-3">
        <input type="date" class="form-control" name="tanggal_lahir" placeholder="Tanggal Lahir">
        </div>
        @error('tanggal_lahir')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               I agree to the <a href="/registerm">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <a href="/login" class="text-center">I already have an account</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->
</div>
</div>
</div>

@endsection