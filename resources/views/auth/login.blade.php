@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="" class="h1"><b>Meet-</b>Upgram</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg"><b>Login</b></p>

      <form action="{{route('login')}}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="email" placeholder="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
        
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
        @enderror
      
        <div class="form-group row">
        <div class="col-8">  
                            <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                
                            </div>
                        </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
            </div>
          <!-- /.col -->
        </div>
      </form>
      @if (Route::has('password.request'))
        <a class="text-left mb-3" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
        </a>
    @endif
      <a href="/register" class="text-left mt-5"><p>I don't have an account</p></a>
</div>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->
</div>
</div>
</div>

@endsection