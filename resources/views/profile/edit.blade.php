@extends('layouts.master')

@section('cardside')
<div class="col-md-2">
<h3><a style="color:blue"><b>Meet-Up</b></a><a style="color:red">gram</a></h3>
<h5> Update Profile </h5>
</div>
@endsection

@section ('main')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<div class="profile-box">
<div class="card card-primary card-outline">
      <div class="card-body box-profile">
        <div class="text-center">
          <img class="profile-user-img img-fluid img-circle"
               src="{{asset('admin/dist/img/AdminLTELogo.png')}}"
               alt="User profile picture">
</div>
<div>
    <form action="/profile/{{$profile->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" value="{{$profile->user->username}}" disabled>
            </div>
            <div class="form-group">
                <label>e-mail</label>
                <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
            </div>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="name" value="{{$profile->name}}">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Biodata</label>
                <textarea name="bio" class="form-control" cols="30" rows="5">{{$profile->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="date" class="form-control" name="tanggal_lahir" value="{{$profile->tanggal_lahir}}">
                @error('tanggal_lahir')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>

@endsection