@extends('layouts.master')

@section('judul')
<h3><a style="color:blue"><b>Meet-Up</b></a><a style="color:red">gram</a></h3>
<h5> My Profile & Posts </h5>
@endsection

@push('style')
<style>
.accordion {
  cursor: pointer;
  width: 100%;
  text-align: left;
  border: none;
  outline: none;
  transition: 0.4s;
}

.panel {
  padding: 0 18px;
  background-color: white;
  display: none;
  overflow: hidden;
}

.fa-heart {
  color: rgb(218, 71, 96);
}
</style>
@endpush

@section('cardside')

  <div class="col-md-3">

    <!-- Profile Image -->
    <div class="card card-primary card-outline">
      <div class="card-body box-profile">
        <div class="text-center">
          <img class="profile-user-img img-fluid img-circle"
               src="{{asset('admin/dist/img/AdminLTELogo.png')}}"
               alt="User profile picture">
        </div>

        <h3 class="profile-username text-center">{{ Auth::user()->profile->name }}</h3>

        <p class="text-muted text-center">&#64;{{ Auth::user()->username }}</p>

        <ul class="list-group list-group-unbordered mb-3">
          <li class="list-group-item">
          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#followers" role="tab" aria-controls="nav-home" aria-selected="true">Followers <span class="badge badge-primary"> {{$user->followers($user->id)->count()}} </span></a>
          </li>
          <li class="list-group-item">
          <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#following" role="tab" aria-controls="nav-profile" aria-selected="false">Following <span class="badge badge-primary"> {{$user->following()->count()}} </span></a>
          </li>
        </ul>

       </div>
       
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

    <!-- About Me Box -->
    <div class="card card-secondary">
      <div class="card-header">
        <h3 class="card-title">About Me</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        
        <strong><i class="far fa-file-alt mr-1"></i> E-mail</strong>

        <p class="text-muted">{{ Auth::user()->email }}</p>

        <hr>
      
        <strong><i class="fas fa-book mr-1"></i> Tanggal Lahir</strong>

        <p class="text-muted">{{ Auth::user()->profile->tanggal_lahir }}</p>

        <hr>

        <strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

        <p class="text-muted">{{ Auth::user()->profile->bio }}</p>

        <hr>
        <form action="/profile/{{$profile->id}}/edit" method="get">
          @csrf
        <button type="submit" class="btn btn-primary btn-block my-3">Update My Profile</button>
        </form>
        <form action="{{route('logout')}}" method="post">
        @csrf
        <button type="submit" class="btn btn-danger btn-block">Logout</button>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
@endsection

@section('main')

<div class="card-header p-2">
  <a class="btn btn-primary" href="/post/create">+ Create Post</a>
</div><!-- /.card-header -->
<div class="card-body">
  <div class="tab-content">
    <div class="active tab-pane" id="activity">
    @forelse (Auth::user()->post as $key=>$value)
          <!-- Post -->
          <div class="post">
            <div class="user-block">
              <img class="img-circle img-bordered-sm" src="{{asset('admin/dist/img/AdminLTELogo.png')}}" alt="user image">
              <span class="username">
                <span class="badge badge-success">&#64;{{Auth::user()->username}}</span>
                <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
              </span>

              <form action="/post/{{$value->id}}/edit" method="GET" class="float-right">
                @csrf
                <button type="submit" class="btn btn-outline-primary mr-2 ml-2"><small>Edit Post</small></button>
              </form>
              <form action="/post/{{$value->id}}" method="POST">
              @csrf
              @method('DELETE')
              <span class="float-right">
              <button type="submit" onclick="return confirm('Apakah kamu yakin hapus?')" class="btn btn-danger"><small>Delete Post</small></button>
              </span>
              </form>

              <span class="description">Shared publicly {{$value->created_at}}</span>
            </div>
            <!-- /.user-block -->
            <div class="mb-3">
              <img class="img-fluid" src="{{asset('images/'.$value->image)}}" alt="Photo">
            </div>
            <p>{{$value->caption}}</p>
    
            @if ($value->show_likepost() === null)
            <form action="/likepost" method="POST">
              @csrf
                <input type="hidden" value="{{$value->id}}" name="post_id">
                <button type="submit" class="btn"><i class="far fa-heart"> {{$value->likerpost()->count()}} </i></button>
                 <button type="submit" class="btn"><i class="far fa-comments">  ({{$value->komentar->count()}})</i></button>
            </form>
            @else
            <form action="/likepost/{{$value->show_likepost()->id}}" method="POST">
              @csrf
              @method('DELETE')
                <button type="submit" class="btn"><i class="fas fa-heart"> {{$value->likerpost()->count()}} </i></button>
                <button type="submit" class="btn"><i class="far fa-comments">  ({{$value->komentar->count()}})</i></button>
            </form>
            @endif

            <h4 style="color:blue">Komentar:</h4>
            @forelse ($value->komentar as $item)
            <div class="card">
            <div class="card-body">
            @if ($item->show_likekomen() === null)
                  <form action="/likekomen" method="POST" class="float-right">
                    @csrf
                    <input type="hidden" value="{{$item->id}}" name="comment_id">
                    <button type="submit" class="btn"><i class="far fa-heart"> {{$item->likerkomen()->count()}} </i></button>
                  </form>
                  @else
                  <form action="/likekomen/{{$item->show_likekomen()->id}}" method="POST" class="float-right">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn"><i class="fas fa-heart"> {{$item->likerkomen()->count()}} </i></button>
                  </form>
                  @endif
                <small><b>&#64;{{$item->user->username}}</b></small>
                <p class="card-text">{{$item->komentar}}</p>
            </div>
            </div>
            @empty
                <h5>Tidak Ada Komentar</h5>
            @endforelse
              
          </div>
          @empty
          <div>
            <h4 class="m-4">Kamu Belum Memiliki Postingan</h4>
          </div>
          <!-- /.post --> 
      @endforelse

    </div>
    <!-- /.tab-pane -->
    <!-- /.tab-pane -->

    
    <!-- /.tab-pane -->
  </div>
  <!-- /.tab-content -->
</div>
@push('script')
<script type="text/javascript" src="{{ asset('js/extras.js') }}" defer></script>
@endpush
@endsection