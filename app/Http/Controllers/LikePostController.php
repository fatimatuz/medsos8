<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\LikePost;
use App\Post;

class LikePostController extends Controller
{
    public function store(Request $request)
    {
        $likepost = new LikePost;
        $user = Auth::user();

        $likepost->user_id = $user->id;
        $likepost->post_id = $request->post_id;

        $likepost->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        $likepost = LikePost::find($id);
        $likepost->delete();
        return redirect()->back();
    }
}
