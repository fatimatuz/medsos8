<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Follow;
use App\LikePost;

class UserController extends Controller
{
    public function index(){
       $users = User::all();
       return view('user.index', compact('users'));
    }

}