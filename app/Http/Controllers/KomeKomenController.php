<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KomenKomen;

class KomeKomenController extends Controller
{
    public function store(Request $request){

        $request->validate([
            'comment_id' => 'required',
            'comment2_id' => 'required'
        ]);

        $komentar2 = new KomenKomen;
        $komentar2->comment_id = $request->comment_id;
        $komentar2->comment2_id = $request->comment2_id;
                
        $komentar2->save();

        return redirect()->back();
    }
}
