<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\LikePost;
use App\Komentar;
use App\LikeKomen;
use File;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::all();
        return view('dashboard', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'caption' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $post = new Post;
        $user = Auth::user();

        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            $post->image = $imageName;
        }

        $post->caption = $request->caption;
        $post->user_id = $user->id;

        $post->save();

        Alert::success('Published', 'Postingan Kamu Berhasil Diunggah');
        return redirect('/home');
    }
    
    public function edit($id){
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request){

        $request->validate([
            'caption'=>'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
    
        $post = Post::find($id);
        $post->caption = $request->caption;
        
        if ($request->has('image')) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            $post->image = $imageName;
        }

        $post->save();
        Alert::success('Edited', 'Postingan Kamu Berhasil Diubah');
        return redirect ('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        if ($post->image !== null) {
            $path = '/images';
            File::delete($path . $post->image);
        }

        Alert::success('Deleted', 'Postingan Kamu Dihapus');
        return redirect('/home');
    }
}