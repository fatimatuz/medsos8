<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\LikeKomen;
use App\Post;

class LikeKomenController extends Controller
{
    public function store(Request $request)
    {
        $likekomen = new LikeKomen;
        $user = Auth::user();

        $likekomen->user_id = $user->id;
        $likekomen->comment_id = $request->comment_id;

        $likekomen->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        $likekomen = LikeKomen::find($id);
        $likekomen->delete();
        return redirect()->back();
    }
}
