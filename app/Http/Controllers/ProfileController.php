<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();
        $post = Post::where('user_id', Auth::id())->first();
        $user = Auth::user();
        return view('profile.index', compact('profile', 'post', 'user'));
    }

    public function show($id)
    {
        $profile = Profile::find($id);
        return view('profile.index', compact('profile'));
    }

    public function edit(){
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name'=>'required',
            'bio'=>'required',
            'tanggal_lahir'=>'required'        
        ]);
    
        $profile = Profile::find($id);
        $profile->name = $request['name'];
        $profile->bio = $request['bio'];
        $profile->tanggal_lahir = $request['tanggal_lahir'];

        $profile->save();

        Alert::success('Profile Updated', 'Profil Kamu Berhasil Diubah');
        return redirect ('/profile');
    }
}

