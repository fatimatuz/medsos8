<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\LikeKomen;

class KomentarController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'komentar' => 'required'
        ]);

        $komentar = new Komentar;
        $komentar->komentar = $request->komentar;
        $komentar->user_id = Auth::id();
        $komentar->post_id = $request->post_id;
        $komentar->save();

        Alert::success('Submitted', 'Komentar Kamu telah Ditambahkan');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $komen = Komentar::find($id);
        $komen->delete();
        return redirect()->back();
    }
}
