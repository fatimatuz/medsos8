<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = "follow";

    protected $fillable = ["user_id", "follower_id"];

    public function follower()
    {
        return $this->belongsTo('App\User');
    }
}
