<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function post()
    {
        return $this->hasMany('App\Post');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

    public function following()
    {
        return $this->hasMany('App\Follow');
    }
    
    public function followers($id)
    {
        $followers = Follow::where('follower_id', $this->id)->get();
        return $followers;
    }

    public function show_follow($id)
    {
        $followers = Follow::where([['user_id', $this->id], ['follower_id', $id]])->first();
        return $followers;
    }
}
