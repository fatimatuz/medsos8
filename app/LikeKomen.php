<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeKomen extends Model
{
    protected $table = 'like_comment';

    protected $fillable = ['user_id', 'comment_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Komentar');
    }
}
