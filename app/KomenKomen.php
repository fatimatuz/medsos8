<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Komentar;

class KomenKomen extends Model
{
    protected $table = 'comment_comment';

    protected $fillable = ['comment_id', 'comment2_id'];

    public function comment()
    {
        return $this->belongsTo('App\Komentar');
    }

    public function getuser($id)
    {
        $comment = Komentar::find($id);
        $user = User::find($comment->user_id);
        dd($user);
        return $user;
    }

    public function getcomment($id)
    {
        $comment = Komentar::find($id);
        dd($comment);
        return $comment;
    }
}
