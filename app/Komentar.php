<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\LikeKomen;

class Komentar extends Model
{
    protected $table = 'comment';

    protected $fillable = ['user_id', 'post_id', 'komentar'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function likekomen()
    {
        return $this->hasMany('App\LikeKomen');
    }

    public function likerkomen()
    {
        $likekomen = LikeKomen::where('comment_id', $this->id)->get();
        return $likekomen;
    }

    public function show_likekomen()
    {
        $likekomen = LikeKomen::where([['user_id', Auth::user()->id], ['comment_id', $this->id]])->first();
        return $likekomen;
    }

    public function komenkomen()
    {
        return $this->hasMany('App\KomenKomen');
    }
}
