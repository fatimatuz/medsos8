<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    protected $table = 'post';

    protected $fillable = ['caption', 'image', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

    public function likepost()
    {
        return $this->hasMany('App\LikePost');
    }

    public function likerpost()
    {
        $likerpost = LikePost::where('post_id', $this->id)->get();
        return $likerpost;
    }

    public function show_likepost()
    {
        $likepost = LikePost::where([['user_id', Auth::user()->id], ['post_id', $this->id]])->first();
        return $likepost;
    }
}
