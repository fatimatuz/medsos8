<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth')->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('profile', 'ProfileController')->only([
        'index', 'show', 'edit', 'update'
    ]);
    Route::resource('post', 'PostController');
    Route::get('/users', 'UserController@index');
    
    Route::resource('komentar', 'KomentarController')->only([
        'store', 'destroy'
    ]);

    Route::resource('follow', 'FollowController')->only([
        'store', 'destroy'
    ]);

    Route::resource('komenkomen', 'KomenKomenController')->only([
        'store'
    ]);

    Route::resource('likepost', 'LikePostController')->only([
        'store', 'destroy'
    ]);

    Route::resource('likekomen', 'LikeKomenController')->only([
        'store', 'destroy'
    ]);
});

Route::get('/registerm', function(){
    return view('terms');
});
